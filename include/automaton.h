#pragma once

#include <utility>

#include "Eigen/Core"
#include "Eigen/Sparse"
#include <iostream>
#include <set>

using namespace Eigen;

typedef Eigen::Triplet<double> T;

struct automaton
{
	using IteratorType = int;

	std::map<int, SparseMatrix<int>> graph;
	MatrixXi success_states;

	int size;

	struct editor
	{
		automaton& self;
		std::map<int, std::vector<T>*> triplets;

		editor(automaton& a) : self(a) {}

		editor* has(int x, int y, int t) 
		{
			if(triplets[t] == NULL) triplets[t] = new std::vector<T>();
			triplets[t]->push_back(T(x, y, 1));
			return this;
		}

		editor* success(int n)
		{
			self.success_states(0, n) = 1;
			return this;
		}

		void done()
		{		
			for(auto it = triplets.begin(); it != triplets.end(); ++it)
			{	
				self.graph[it->first] = SparseMatrix<int>(self.size, self.size);
				self.graph[it->first].setFromTriplets(it->second->begin(), it->second->end());
				delete it->second;
			}
		}
	};

	editor default_editor;
	
	automaton(int n)
		: graph(), success_states(1, n), size(n), default_editor(editor(*this))
	{
	}

	editor* edit()
	{
		return &default_editor;
	}
};

template<size_t Size>
struct iso
{
	using IteratorType = std::array<int, Size>;

	automaton* tracks[Size];

	int size;
	
	struct editor
	{
		iso& self;
		
		editor(iso& i) : self(i) {}

		editor* has(int x, int y, std::array<int, Size> t)
		{
			for(int i = 0; i < Size; i++)
			{
				self.tracks[i]->edit()->has(x, y, t[i]);
			}
			return this;
		}

		editor* success(int n)
		{
			for(int i = 0; i < Size; i++)
			{
				self.tracks[i]->edit()->success(n);
			}
			return this;
		}

		void done()
		{
			for(int i = 0; i < Size; i++)
			{
				self.tracks[i]->edit()->done();
			}
		}
	};

	editor default_editor;

	iso(int n)
		: default_editor(editor(*this)), size(n)
	{
		for(int i = 0; i < Size; i++)
		{
			tracks[i] = new automaton(n);
		}
	}

	~iso()
	{
		for(int i = 0; i < Size; i++)
		{
			delete tracks[i];
		}
	}
	editor* edit()
	{
		return &default_editor;
	}
};

template<typename Structure>
struct abstract_cursor {
        virtual void traverse(typename Structure::IteratorType t) = 0;
        virtual bool is_success() = 0;
};

template<typename Structure>
struct cursor {};

template<>
struct cursor<automaton> : public abstract_cursor<automaton>
{
	automaton& base;
	MatrixXi transition, state;
	int sink;

	cursor(automaton& aut) : 
		base(aut), transition(1, aut.size), state(1, aut.size), sink(1)
	{
		transition.setZero();
		state.setZero();
		state(0, 0) = 1;
	}
	
	void traverse(int n)
	{
		if(sink != 0) {
			transition = state;
			if(base.graph.count(n)) {
				state = transition * base.graph[n];
				std::cout << state << std::endl;
				sink = state.sum();
				std::cout << sink << std::endl;
			} else {
				state = MatrixXi::Zero(1, base.size);
				sink = 0;
			}
		}
	}

	bool is_success() 
	{
		return (state * base.success_states.transpose()).sum() > 0;
	}
};

template<size_t Size>
struct cursor<iso<Size>> : public abstract_cursor<iso<Size>>
{
	cursor<automaton>* tracks[Size];

	cursor(iso<Size>& aut)
	{
		for(int i = 0; i < Size; i++)
		{
			tracks[i] = new cursor<automaton>(*(aut.tracks[i]));
		}
	}

	void traverse(std::array<int, Size> n)
	{
		for(int i = 0; i < Size; i++)
		{
			tracks[i]->traverse(n[i]);
		}
	}

	bool is_success()
	{
		for(int i = 0; i < Size; i++)
		{
			if(!tracks[i]->is_success()) return false;
		}

		return true;
	}
};

template<typename Structure>
cursor<Structure> iterator(Structure& s)
{
	return cursor<Structure>(s);
};


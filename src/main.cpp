
#include <automaton.h>

int main()
{
	automaton a(3);
	a.edit()
	  ->has(0, 1, 'a')
	  ->has(1, 2, 'b')
	  ->has(2, 2, 'b')
	  ->success(2)
	  ->done();

	auto c = iterator(a);
	c.traverse('a');
	c.traverse('b');
	c.traverse('b'); 
	std::cout << c.is_success() << std::endl;

	iso<2> b(3);
	b.edit()
	  ->has(0, 1, { 'a', 'a' })
	  ->has(1, 2, { 'b', 'b' })
	  ->has(2, 2, { 'b', 'b' })
	  ->has(2, 2, { ' ', ' ' })
	  ->success(2)
	  ->done();

	auto d = iterator(b);
	std::string x = "abb ";
	std::string y = "abb ";

	for(int i = 0; i < x.length(); i++)
	{
		d.traverse({{ x[i], y[i] }});	
	}
	
	std::cout << "=================!!!================" << std::endl;
	std::cout << "are these words the same? " << (d.is_success() ? "yes" : "no") << std::endl;
}
